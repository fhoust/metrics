# Observability Env

A complete observability env using docker to collect logs and metrics

![Grafana](./miscs/grafana.png)

## How to Deploy

### Requirements

- [docker](https://docs.docker.com/engine/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

### Running

In order to collect the logs, you will need to set docker log-driver to loki and create a external network:

``` bash
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
docker network create loki
```

Then deploy the compose:

``` bash
docker-compose up -d
```

## Collecting other containers logs

Just add the flags:

``` bash
--log-driver=loki 
--log-opt loki-url="http://localhost:3100/loki/api/v1/push"
```

For example:

``` bash
docker run --name microblog -d -p 8000:5000 --rm --log-driver=loki \
    --log-opt loki-url="http://localhost:3100/loki/api/v1/push" --name microblog \
    demo.goharbor.io/cobrainer-ci/microblog:0.0.1
```

## Accessing

All modules will be deployed in localhost, so if you have access to localhost use this links to access:

- [Grafana](http://localhost:3000)
- [Prometheus](http://localhost:9090)
- [Loki Api](http://localhost:3100)
- [Node_exporter](http://localhost:9100)

## Grafana first access

In the first access to Grafana use: admin/admin

### Creating datasources

To use dashboards you need to have datasources:

- Go to **Configuration** > **Datasources** > **Add data source**2.
    1. Prometheus: Add **http://prometheus:9090** in URL and save
    2. Loki: Add **http://grafana-loki:3100** in URL and save

### Adding Dashboards

Look in **dashs** folders to get a list of ids and json templates for log and alert dashboards. [How export and import dashs](https://grafana.com/docs/grafana/latest/dashboards/export-import/)
